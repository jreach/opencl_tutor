#include <vector>
#include <algorithm>
#include <iostream>

#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/functional/math.hpp>

#include "swarm_tick_tock.hpp"

namespace compute = boost::compute;

using namespace std;



int main()
{


    swarm::SwarmTickTock tt;
    // get default device and setup context
    compute::device device = compute::system::default_device();
    compute::context context(device);
    compute::command_queue queue(context, device);

    // generate random data on the host
    std::vector<float> host_vector(10000000);
    std::generate(host_vector.begin(), host_vector.end(), rand);

    tt.tocktick();
    cout << "GPU START" << endl;
    // create a vector on the device
    compute::vector<float> device_vector(host_vector.size(), context);


    // transfer data from the host to the device
    compute::copy(
        host_vector.begin(), host_vector.end(), device_vector.begin(), queue
    );

    // calculate the square-root of each element in-place
    compute::transform(
            device_vector.begin(),
            device_vector.end(),
            device_vector.begin(),
            compute::sqrt<float>(),
            queue
            );

    // copy values back to the host
    compute::copy(
        device_vector.begin(), device_vector.end(), host_vector.begin(), queue
    );

    //for( auto& hv : host_vector ) cout << hv << endl;
    
    
    cout << "GPU END: " << tt.tocktick() * 1000 << endl;

    return 0;
}
