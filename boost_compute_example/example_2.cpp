#include <vector>
#include <algorithm>
#include <iostream>

#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/functional/math.hpp>

#include "swarm_tick_tock.hpp"

namespace compute = boost::compute;

using namespace std;



int main()
{
    swarm::SwarmTickTock tt;

    // get default device and setup context
    compute::device device = compute::system::default_device();
    compute::context context(device);
    compute::command_queue queue(context, device);

    // generate random data on the host
    //std::vector<float> host_vector(10000000);
    
    int numEntries = 1000000;
    std::vector<int> host_vector(numEntries);

    for( int i = 0; i < numEntries; ++i ) {
        host_vector[i] = i;
    }

    tt.tocktick();
    cout << "GPU START: Example2" << endl;

    // create a vector on the device
    compute::vector<int> device_vector(host_vector.size(), context);

    //Use this to build a function kinda like a function<int(int)> for functional
    BOOST_COMPUTE_FUNCTION(int, add_four, (int x),
    {
        //Absurd data computations
        for( int i = 0; i < 1000000; ++i ) {
            for( int j = 0; j < 1000000; ++j ) {
                x += 4;
                //x = x - 4;
                //x = x * 4;
                //x = x / 4;
            }
        }

        return x + 4;
    });


    // transfer data from the host to the device
    compute::copy(
        host_vector.begin(), host_vector.end(), device_vector.begin(), queue
    );


    //Execute
    boost::compute::transform(
            device_vector.begin(), 
            device_vector.end(), 
            device_vector.begin(), add_four, queue);

    cout << "GPU END: " << tt.tocktick() * 1000 << endl;



    // copy values back to the host
    compute::copy(
        device_vector.begin(), device_vector.end(), host_vector.begin(), queue
    );


    //for( auto& hv : host_vector ) cout << hv << endl;

    return 0;
}
