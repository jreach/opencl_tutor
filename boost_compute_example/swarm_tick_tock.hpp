#pragma once

#include <iostream>
#include <string>
#include <mutex>
#include <chrono>
#include <vector>

namespace swarm {
	using namespace std;
	using namespace chrono;

	class SwarmTickTock {
	public:
		SwarmTickTock();

		void tick();
		double tock();
		double tocktick();

	private:
		mutable mutex _mtx;
		time_point<high_resolution_clock> _start;
		time_point<high_resolution_clock> _end;
	};
}

namespace swarm {
	using namespace std;
	using namespace chrono;

	class SwarmFPS : public SwarmTickTock {
	public:
		SwarmFPS( int numSamples = 10 );

		double frame();

	private:
		int mNumSamples;
		int mCurrentSample = 0;

		vector<double> mSamples;

		double mFPS = 0;
	};
}

namespace swarm {
	SwarmTickTock::SwarmTickTock() {
		_start = high_resolution_clock::now();
	}

	void SwarmTickTock::tick() {
		lock_guard<mutex> lock(_mtx);
		_start = high_resolution_clock::now();
	}

	double SwarmTickTock::tock() {
		lock_guard<mutex> lock(_mtx);
		_end = high_resolution_clock::now();
		duration<double> seconds = _end - _start;
		return seconds.count();
	}

	double SwarmTickTock::tocktick() {
		double seconds = tock();
		tick();
		return seconds;
	}
}


namespace swarm {

	SwarmFPS::SwarmFPS(int numSamples) {
		mNumSamples = numSamples;
		mSamples = vector<double>(mNumSamples, 0);
	}

	double SwarmFPS::frame() {
		mSamples[mCurrentSample] = tocktick();
		if (mCurrentSample == mNumSamples) {
			mCurrentSample = 0;
			mFPS = 0;

			for (int i = 0; i < mNumSamples; ++i) {
				mFPS += mSamples[i];
			}
			mFPS /= mNumSamples;

			mFPS = 1.0 / mFPS;
		}
		else {
			++mCurrentSample;
		}

		return mFPS;
	}
}
