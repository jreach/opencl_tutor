#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#include "../../opencl/src/simplex_noise_seed.cl"

struct DVec3 {
	double x;
	double y;
	double z;
};

struct Vec3 {
	float x;
	float y;
	float z;
};

struct VertDataPNCM {
	struct DVec3 pos;
	struct DVec3 normal;
	struct DVec3 color;
	double noise;
	double density;
	double material;
};

float surfaceNoise( float height_component, float rate_change, float x, float y, float z );

float surfaceNoiseSeeded( 
    float height_component, 
    float rate_change, 
    float x, float y, float z, 
    __global int* seedBuffer );


__kernel void produceChunk3DSimple(
    __global struct Vec3* pBuffer, 
    __global float* nBuffer, 
    struct DVec3 pos, double resolution, double range )
{
    float tx = get_global_id(0);
    float ty = get_global_id(1);
    float tz = get_global_id(2);

    //float nX = tx * resolution + pos.x + -(range * resolution / 2) + resolution / 2;
    //float nY = ty * resolution + pos.y + -(range * resolution / 2) + resolution / 2;
    //float nZ = tz * resolution + pos.z + -(range * resolution / 2) + resolution / 2;


    float nX = tx * resolution + pos.x;
    float nY = ty * resolution + pos.y;
    float nZ = tz * resolution + pos.z;

    int index = tx * range * range + ty * range + tz;

    pBuffer[index].x = nX;
    pBuffer[index].y = nY;
    pBuffer[index].z = nZ;
    nBuffer[index] = surfaceNoise( 0, 0, nX, 0, nZ );
}

__kernel void produceChunk3DSimpleSeeded(
    __global struct Vec3* pBuffer, 
    __global float* nBuffer, 
    __global int* seedBuffer, 
    struct DVec3 pos, double resolution, double range )
{
    float tx = get_global_id(0);
    float ty = get_global_id(1);
    float tz = get_global_id(2);

    //float nX = tx * resolution + pos.x + -(range * resolution / 2) + resolution / 2;
    //float nY = ty * resolution + pos.y + -(range * resolution / 2) + resolution / 2;
    //float nZ = tz * resolution + pos.z + -(range * resolution / 2) + resolution / 2;

    float nX = tx * resolution + pos.x;
    float nY = ty * resolution + pos.y;
    float nZ = tz * resolution + pos.z;

    int index = tx * range * range + ty * range + tz;

    pBuffer[index].x = nX;
    pBuffer[index].y = nY;
    pBuffer[index].z = nZ;
    nBuffer[index] = surfaceNoiseSeeded( 0, 0, nX, 0, nZ, seedBuffer );
    //nBuffer[index] = surfaceNoiseSeeded( 0, 0, nX, nY, nZ, seedBuffer );
}

__kernel void filterChunk3DSimple(
    __global struct Vec3* pBuffer, 
    __global float* nBuffer, 
    __global char* sBuffer, 
    struct DVec3 pos, double resolution, double range )
{
    int tx = get_global_id(0);
    int ty = get_global_id(1);
    int tz = get_global_id(2);

	int xx = 0;
	int yy = 0;
	int zz = 0;
	int spanNeg = 1;
	int spanPos = 1;
	char in = 0;
	char out = 0;
	for (xx = -spanNeg; xx <= spanPos; ++xx) {
		for (yy = -spanNeg; yy <= spanPos; ++yy) {
			for (zz = -spanNeg; zz <= spanPos; ++zz) {
				if( tx > 0 && tx < range - 1 && ty > 0 && ty < range - 1 &&  tz > 0 && tz < range - 1 ) {
					if( xx == 0 && yy == 0 && zz == 0 ) {
						continue;
					}
					int ii = (tx + xx) * range * range + (ty + yy) * range + (tz + zz);
					if (nBuffer[ii] - (pBuffer[ii].y) < 0) {
						out = 1;	
					}
					else {
						in = 1;
					}
				}
			}
		}
	}

	int ii = tx * range * range + ty * range + tz;
	sBuffer[ii] = 0;
	if ( in == 1 && out == 1 ) {
		sBuffer[ii] = 1;
	}
	//else {
	//	sBuffer[ii] = 0;
	//}
}

__kernel void heightMapSimple(
    __global struct DVec3* pBuffer, 
    struct DVec3 pos, double resolution, double range )
{
    double tx = get_global_id(0);
    double tz = get_global_id(1);

    //double nX = tx * resolution + pos.x + -(range * resolution / 2) + resolution / 2;
    //double nZ = tz * resolution + pos.z + -(range * resolution / 2) + resolution / 2;

    float nX = tx * resolution + pos.x;
    //float nY = ty * resolution + pos.y;
    float nZ = tz * resolution + pos.z;

    int index = tx * range + tz;

    pBuffer[index].x = nX;
    pBuffer[index].y = surfaceNoise( 0, 0, nX, 0, nZ );
    pBuffer[index].z = nZ;
}

__kernel void heightMapSimpleSeeded(
    __global struct DVec3* pBuffer, 
    __global int* seedBuffer, 
    struct DVec3 pos, double resolution, double range )
{
    double tx = get_global_id(0);
    double tz = get_global_id(1);

    //double nX = tx * resolution + pos.x + -(range * resolution / 2) + resolution / 2;
    //double nZ = tz * resolution + pos.z + -(range * resolution / 2) + resolution / 2;

    float nX = tx * resolution + pos.x;
    //float nY = ty * resolution + pos.y;
    float nZ = tz * resolution + pos.z;

    int index = tx * range + tz;

    pBuffer[index].x = nX;
    pBuffer[index].y = surfaceNoiseSeeded( 0, 0, nX, 0, nZ, seedBuffer );
    pBuffer[index].z = nZ;
}

float surfaceNoise( float height_component, float rate_change, float x, float y, float z ) {


    //This is interesting, blending needs to happen on both sides of this.
    //These graphics illustrate this perfectly, looks like a crown!
    struct NoiseData3 spikey = { 15, 0.58, 1.0 / 100.0, 0, 300, x, y, z, 3245 };
    //struct NoiseData3 spikey = { 1, 1, 1.0 / 100.0, 0, 100, x, y, z, 33333 };
    struct NoiseData3 smallHills = { 1, 1, 1.0 / 100.0, 0, 100, x, y, z, 33333 };
    struct NoiseData3 selectorData = { 1, 1, 1.0 / 1000.0, 0, 1000, x, y, z, 5532 };

    float noise;
    float lowlowBlend = 350;
    float lowBlend = 400;
    float highBlend = 500;
    float selector = scaled_octave_noise_3dd( selectorData );
    if( selector > lowlowBlend && selector < lowBlend  ) {
        float n1 = scaled_octave_noise_3dd( spikey );
        float n2 = scaled_octave_noise_3dd( smallHills );
        noise = lerp( n2, n1, (selector - lowlowBlend) / (lowBlend - lowlowBlend) );
    }
    else if( selector > lowBlend && selector < highBlend  ) {
        float n1 = scaled_octave_noise_3dd( spikey );
        float n2 = scaled_octave_noise_3dd( smallHills );
        noise = lerp( n1, n2, (selector - lowBlend) / (highBlend - lowBlend) );
    }
    else {
        noise = scaled_octave_noise_3dd( smallHills );
    }
    /*
    else if( selector > highBlend ) {
        float n1 = scaled_octave_noise_3dd( spikey );
        float n2 = scaled_octave_noise_3dd( smallHills );
        //noise = lerp( n1, n2, selector / 1000.0 );
        //noise = lerp( n1, n2, (selector - lowBlend) / (selector - highBlend) );
        noise = lerp( n1, n2, (selector - highBlend) / (1000.0 - highBlend) );
    }
    else { // selector < lowBlend
        float n1 = scaled_octave_noise_3dd( spikey );
        float n2 = scaled_octave_noise_3dd( smallHills );
        //noise = lerp( n1, n2, selector / 1000.0 );
        noise = lerp( n1, n2, ( selector ) / ( lowBlend ) );
    }
    */

    return noise;

}

float surfaceNoiseSeeded( 
    float height_component, 
    float rate_change, 
    float x, float y, float z,
    __global int* seedBuffer ) 
{


    //This is interesting, blending needs to happen on both sides of this.
    //These graphics illustrate this perfectly, looks like a crown!
    struct NoiseData3 spikey = { 15, 0.58, 1.0 / 100.0, 0, 300, x, y, z, 1 };
    struct NoiseData3 smallHills = { 1, 1, 1.0 / 100.0, 0, 100, x, y, z, 2 };
    struct NoiseData3 selectorData = { 1, 1, 1.0 / 1000.0, 0, 1000, x, y, z, 3 };

    float noise;
    float lowlowBlend = 350;
    float lowBlend = 400;
    float highBlend = 500;

    float selector = scaled_octave_noise_3dds( selectorData, seedBuffer );
    if( selector > lowlowBlend && selector < lowBlend  ) {
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        noise = lerp( n2, n1, (selector - lowlowBlend) / (lowBlend - lowlowBlend) );
    }
    else if( selector > lowBlend && selector < highBlend  ) {
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        noise = lerp( n1, n2, (selector - lowBlend) / (highBlend - lowBlend) );
    }
    else {
        noise = scaled_octave_noise_3dds( smallHills, seedBuffer );
    }

    return noise;
}

/*
float surfaceNoiseSeeded( 
    float height_component, 
    float rate_change, 
    float x, float y, float z,
    __global int* seedBuffer ) 
{


    //This is interesting, blending needs to happen on both sides of this.
    //These graphics illustrate this perfectly, looks like a crown!
    struct NoiseData3 spikey = { 15, 0.58, 1.0 / 100.0, 0, 300, x, y, z, 1 };
    struct NoiseData3 spikey2 = { 15, 0.58, 1.0 / 10.0, 0, 300, x, y, z, 4 };
    struct NoiseData3 smallHills = { 1, 1, 1.0 / 100.0, 0, 100, x, y, z, 2 };
    struct NoiseData3 selectorData = { 1, 1, 1.0 / 1000.0, 0, 1000, x, y, z, 3 };

    float noise;
    float lowlowBlend = 350;
    float lowBlend = 400;
    float highBlend = 500;

    float lowBlend2 = 500;
    float highBlend2 = 600;

    float selector = scaled_octave_noise_3dds( selectorData, seedBuffer );
    if( selector > lowlowBlend && selector < lowBlend  ) {
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        noise = lerp( n2, n1, (selector - lowlowBlend) / (lowBlend - lowlowBlend) );
    }
    else if( selector > lowBlend && selector < highBlend  ) {
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        noise = lerp( n1, n2, (selector - lowBlend) / (highBlend - lowBlend) );
    }
    else if( selector > lowBlend2 && selector < highBlend2  ) {
    	float n1 = scaled_octave_noise_3dds( spikey2, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        noise = lerp( n1, n2, (selector - lowBlend) / (highBlend - lowBlend) );
    }
    else {
        noise = scaled_octave_noise_3dds( smallHills, seedBuffer );
    }

    return noise;

}
*/


    /*
    else if( selector > highBlend ) {
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        //noise = lerp( n1, n2, selector / 1000.0 );
        //noise = lerp( n1, n2, (selector - lowBlend) / (selector - highBlend) );
        noise = lerp( n1, n2, (selector - highBlend) / (1000.0 - highBlend) );
    }
    else { // selector < lowBlend
        float n1 = scaled_octave_noise_3dds( spikey, seedBuffer );
        float n2 = scaled_octave_noise_3dds( smallHills, seedBuffer );
        //noise = lerp( n1, n2, selector / 1000.0 );
        noise = lerp( n1, n2, ( selector ) / ( lowBlend ) );
    }
    */

