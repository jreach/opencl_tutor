#include <iostream>
#include <fstream>
#include <string>

// in /opt/AMD* 
#include <CL/cl.hpp>

using namespace std;


//Example compilation
//g++ -std=c++11 -I/opt/AMDAPPSDK-3.0/include/ -L/opt/AMDAPPSDK-3.0/lib/x86_64/sdk/ opencl_demo.cpp -o test -lOpenCL



cl_int _err;
vector< cl::Platform > _platformList;
cl::Context _context;
vector<cl::Device> _devices;
cl::Program _program;
ofstream _log;

void checkErr(cl_int err, const char * name) {
    if (err != CL_SUCCESS) {
        _log << "ERROR: " << name << " (" << err << ")" << std::endl;
        exit(EXIT_FAILURE);
        throw;
    }
}


void compileProgram(string name) {

    std::ifstream file(name.c_str());

    checkErr(file.is_open() ? CL_SUCCESS : -1, name.c_str());

    std::string prog(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));

    cl::Program::Sources source(1, std::make_pair(prog.c_str(), prog.length() + 1));

    _program = cl::Program(_context, source);

    //_err = _program.build(devices,"");
    if (_program.build({ _devices }) != CL_SUCCESS)
    {
        _log << " Error building: " <<
            _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_devices[0]) << "\n";
        _log.flush();

        /*
           const vector<size_t> ptx_size = _program.getInfo<CL_PROGRAM_BINARIES>();
           _program.getInfo<string>(CL_PROGRAM_BINARIES, &ptx);
           _log << "Source: " << ptx << "\n";
           _log.flush();
           exit(1);
           */

        //clGetProgramInfo( _program(), CL_PROGRAM_BINARY_SIZES);
        // Calculate how big the binary is
        cl_int ciErrNum;
        int kernel_length;
        ciErrNum = clGetProgramInfo(_program(), CL_PROGRAM_BINARY_SIZES, sizeof(size_t),
                &kernel_length,
                NULL);

        char* bin;
        bin = (char*)malloc(sizeof(char)*kernel_length);
        ciErrNum = clGetProgramInfo(_program(), CL_PROGRAM_BINARIES, kernel_length, &bin, NULL);

        for (int i = 0; i < kernel_length; ++i)
            _log << bin[i];
    }
}




int main() {

    cl_int err;
    //setenv("CUDA_CACHE_DISABLE", "1", 1);

    //This is an AMD build, who knows what will go wrong!
    //_putenv("CUDA_CACHE_DISABLE=1");

    _log.open("log.txt", std::ofstream::out | std::ofstream::app);

    cl::Platform::get(&_platformList);
    checkErr(_platformList.size() != 0 ? CL_SUCCESS : -1, "cl::Platform::get");
    _log << "Platform number is: " << _platformList.size() << std::endl;

    vector<string> vendors;
    std::string platformVendor;
    for (auto it = _platformList.begin(); it != _platformList.end(); ++it) {
        it->getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
        _log << "Platform is by: " << platformVendor << "\n";
        vendors.push_back(platformVendor);
    }

    int platformID = 0;
    for (int i = 0; i < vendors.size(); ++i) {
        if (vendors[i] == "NVIDIA Corporation") {
            platformID = i;
            break;
        }
        else if (vendors[i] == "Advanced Micro Devices, Inc.") {
            platformID = i;
            break;
        }
    }
    //_platformList[0].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
    //std::cerr << "Platform is by: " << platformVendor << "\n";

    cl_context_properties cprops[3] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)(_platformList[platformID])(), 0
    };

    _context = cl::Context(
            //CL_DEVICE_TYPE_GPU,
            CL_DEVICE_TYPE_CPU,
            cprops,
            NULL,
            NULL,
            &err);

    checkErr(err, "Context::Context()");

    _devices = _context.getInfo<CL_CONTEXT_DEVICES>();
    checkErr(_devices.size() > 0 ? CL_SUCCESS : -1, "_devices.size() > 0");

    string units;
    cl_uint message;
    size_t stupid;
    for (int i = 0; i < _devices.size(); ++i) {
        _devices[i].getInfo((cl_device_info)CL_DEVICE_MAX_COMPUTE_UNITS, &units);
        _log << "Units: " << platformVendor << "\n";
        clGetDeviceInfo(_devices[i](), CL_DEVICE_MAX_COMPUTE_UNITS, 8, &message, &stupid);
        _log << message << endl;
    }

    compileProgram("noise_gen_seed.cl");


    return 0;
}
