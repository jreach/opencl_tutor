#include "opencl_manager.hpp"

#include "game.hpp"

namespace swarm {
	OpenCLManager::OpenCLManager(Game& game, int chunkSide) :
		_game( game), _chunkSide(chunkSide), _chunkSize(chunkSide*chunkSide*chunkSide)
	{

		//Induce seed generation
		vector<int>& seedBuffer = _game.noise().getPermBundle();
		int numPerms = 1000 * 512;

		cl_int err;
		//setenv("CUDA_CACHE_DISABLE", "1", 1);
		_putenv("CUDA_CACHE_DISABLE=1");

		_log.open("log.txt", std::ofstream::out | std::ofstream::app);

		cl::Platform::get(&_platformList);
		checkErr(_platformList.size() != 0 ? CL_SUCCESS : -1, "cl::Platform::get");
		_log << "Platform number is: " << _platformList.size() << std::endl;

		vector<string> vendors;
		std::string platformVendor;
		for (auto it = _platformList.begin(); it != _platformList.end(); ++it) {
			it->getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
			_log << "Platform is by: " << platformVendor << "\n";
			vendors.push_back(platformVendor);
		}

		int platformID = 0;
		for (int i = 0; i < vendors.size(); ++i) {
			if (vendors[i] == "NVIDIA Corporation") {
				platformID = i;
				break;
			}
		}

		//_platformList[0].getInfo((cl_platform_info)CL_PLATFORM_VENDOR, &platformVendor);
		//std::cerr << "Platform is by: " << platformVendor << "\n";

		cl_context_properties cprops[3] = {
			CL_CONTEXT_PLATFORM, (cl_context_properties)(_platformList[platformID])(), 0
		};

		_context = cl::Context(
			CL_DEVICE_TYPE_GPU,
			cprops,
			NULL,
			NULL,
			&err);
		checkErr(err, "Context::Context()");

		_devices = _context.getInfo<CL_CONTEXT_DEVICES>();
		checkErr(_devices.size() > 0 ? CL_SUCCESS : -1, "_devices.size() > 0");

		string units;
		cl_uint message;
		size_t stupid;
		for (int i = 0; i < _devices.size(); ++i) {
			_devices[i].getInfo((cl_device_info)CL_DEVICE_MAX_COMPUTE_UNITS, &units);
			_log << "Units: " << platformVendor << "\n";
			clGetDeviceInfo(_devices[i](), CL_DEVICE_MAX_COMPUTE_UNITS, 8, &message, &stupid);
			_log << message << endl;
		}

		compileProgram("../cl/noise_gen_seed.cl");
	}

	void OpenCLManager::compileProgram(string name) {

		std::ifstream file(name.c_str());

		checkErr(file.is_open() ? CL_SUCCESS : -1, name.c_str());

		std::string prog(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));

		cl::Program::Sources source(1, std::make_pair(prog.c_str(), prog.length() + 1));

		_program = cl::Program(_context, source);

		//_err = _program.build(devices,"");
		if (_program.build({ _devices }) != CL_SUCCESS)
		{
			_log << " Error building: " <<
				_program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_devices[0]) << "\n";
			_log.flush();

			/*
			const vector<size_t> ptx_size = _program.getInfo<CL_PROGRAM_BINARIES>();
			_program.getInfo<string>(CL_PROGRAM_BINARIES, &ptx);
			_log << "Source: " << ptx << "\n";
			_log.flush();
			exit(1);
			*/

			//clGetProgramInfo( _program(), CL_PROGRAM_BINARY_SIZES);
			// Calculate how big the binary is
			cl_int ciErrNum;
			int kernel_length;
			ciErrNum = clGetProgramInfo(_program(), CL_PROGRAM_BINARY_SIZES, sizeof(size_t),
				&kernel_length,
				NULL);

			char* bin;
			bin = (char*)malloc(sizeof(char)*kernel_length);
			ciErrNum = clGetProgramInfo(_program(), CL_PROGRAM_BINARIES, kernel_length, &bin, NULL);

			for (int i = 0; i < kernel_length; ++i)
				_log << bin[i];
		}
	}

	cl::Context* OpenCLManager::getContext() {
		return &_context;
	}

	void OpenCLManager::checkErr(cl_int err, const char * name) {
		if (err != CL_SUCCESS) {
			_log << "ERROR: " << name << " (" << err << ")" << std::endl;
			exit(EXIT_FAILURE);
			throw;
		}
	}

	void OpenCLManager::produceChunk3DSimple(
		glm::vec3* pBuffer, float* nBuffer,
		glm::dvec3 pos, double resolution, double range )
	{
		lock_guard<mutex> lock( _mtx );
		cl_int err;
		cl::Buffer outPBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * range * sizeof(glm::vec3)),
			pBuffer,
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outPBuffer()");

		cl::Buffer outNBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * range * sizeof(float)),
			nBuffer,
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outNBuffer()");

		cl::Kernel kernel(_program, "produceChunk3DSimple", &err);
		uint32_t ii = 0;
		err |= kernel.setArg(ii++, outPBuffer);
		err |= kernel.setArg(ii++, outNBuffer);
		err |= kernel.setArg(ii++, pos);
		err |= kernel.setArg(ii++, resolution);
		err |= kernel.setArg(ii++, range);
		checkErr(err, "Kernel::setArg()");



		cl::CommandQueue queue(_context, _devices[0], 0, &err);
		checkErr(err, "produceChunk3DSimple::CommandQueue::CommandQueue()");
		cl::Event event;
		err = queue.enqueueNDRangeKernel(
			kernel,
			cl::NullRange,
			cl::NDRange((size_t)range, (size_t)range, (size_t)range),
			cl::NullRange,
			NULL,
			&event);
		checkErr(err, "produceChunk3DSimple::ComamndQueue::enqueueNDRangeKernel()");


		event.wait();

		err = queue.enqueueReadBuffer(
			outPBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>( range * range * range * sizeof(glm::vec3)),
			pBuffer);
		checkErr(err, "ComamndQueue::enqueueReadBuffer::outPBuffer()");

		err = queue.enqueueReadBuffer(
			outNBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * range * sizeof(float) ),
			nBuffer);
		checkErr(err, "produceChunk3DSimple::ComamndQueue::enqueueReadBuffer::outNBuffer()");
	}

	void OpenCLManager::filterChunk3DSimple(
		glm::vec3* pBuffer, float* nBuffer, char* sBuffer,
		glm::dvec3 pos, double resolution, double range )
	{
		lock_guard<mutex> lock( _mtx );
		cl_int err;
		cl::Buffer outPBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * range * sizeof(glm::vec3)),
			pBuffer,
			&err);
		checkErr(err, "filterChunk3DSimple::Buffer::outPBuffer()");

		cl::Buffer outNBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * range * sizeof(float)),
			nBuffer,
			&err);
		checkErr(err, "filterChunk3DSimple::Buffer::outNBuffer()");

		cl::Buffer outSBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * range * sizeof(char)),
			sBuffer,
			&err);
		checkErr(err, "filterChunk3DSimple::Buffer::outSBuffer()");

		cl::Kernel kernel(_program, "filterChunk3DSimple", &err);
		uint32_t ii = 0;
		err |= kernel.setArg(ii++, outPBuffer);
		err |= kernel.setArg(ii++, outNBuffer);
		err |= kernel.setArg(ii++, outSBuffer);
		err |= kernel.setArg(ii++, pos);
		err |= kernel.setArg(ii++, resolution);
		err |= kernel.setArg(ii++, range);
		checkErr(err, "filterChunk3DSimple::Kernel::setArg()");



		cl::CommandQueue queue(_context, _devices[0], 0, &err);
		checkErr(err, "filterChunk3DSimple::CommandQueue::CommandQueue()");
		cl::Event event;
		err = queue.enqueueNDRangeKernel(
			kernel,
			cl::NullRange,
			cl::NDRange((size_t)range, (size_t)range, (size_t)range),
			cl::NullRange,
			NULL,
			&event);
		checkErr(err, "filterChunk3DSimple::ComamndQueue::enqueueNDRangeKernel()");


		event.wait();

		err = queue.enqueueReadBuffer(
			outPBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>( range * range * range * sizeof(glm::vec3)),
			pBuffer);
		checkErr(err, "filterChunk3DSimple::ComamndQueue::enqueueReadBuffer::outPBuffer()");

		err = queue.enqueueReadBuffer(
			outNBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * range * sizeof(float) ),
			nBuffer);
		checkErr(err, "filterChunk3DSimple::ComamndQueue::enqueueReadBuffer::outNBuffer()");

		err = queue.enqueueReadBuffer(
			outSBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * range * sizeof(char) ),
			sBuffer);
		checkErr(err, "filterChunk3DSimple::ComamndQueue::enqueueReadBuffer::outSBuffer()");
	}

	void OpenCLManager::heightMapSimple(
		glm::dvec3* pBuffer, glm::dvec3 pos, double resolution, double range )
	{
		lock_guard<mutex> lock( _mtx );
		cl_int err;
		cl::Buffer outPBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>( range * range * sizeof(glm::dvec3) ),
			pBuffer,
			&err);
		checkErr(err, "heightMapSimple::Buffer::outPBuffer()");

		cl::Kernel kernel(_program, "heightMapSimple", &err);
		uint32_t ii = 0;
		err |= kernel.setArg(ii++, outPBuffer);
		err |= kernel.setArg(ii++, pos);
		err |= kernel.setArg(ii++, resolution);
		err |= kernel.setArg(ii++, range);
		checkErr(err, "heightMapSimple::Kernel::setArg()");


		cl::CommandQueue queue(_context, _devices[0], 0, &err);
		checkErr(err, "heightMapSimple::CommandQueue::CommandQueue()");
		cl::Event event;
		err = queue.enqueueNDRangeKernel(
			kernel,
			cl::NullRange,
			cl::NDRange((size_t)range, (size_t)range ),
			cl::NullRange,
			NULL,
			&event);
		checkErr(err, "heightMapSimple::ComamndQueue::enqueueNDRangeKernel()");


		event.wait();

		err = queue.enqueueReadBuffer(
			outPBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>( range * range * sizeof(glm::dvec3) ),
			pBuffer);
		checkErr(err, "heightMapSimple::ComamndQueue::enqueueReadBuffer::outPBuffer()");
	}

	void OpenCLManager::produceChunk3DSimpleSeeded(
		glm::vec3* pBuffer, float* nBuffer,
		glm::dvec3 pos, double resolution, double range )
	{
		lock_guard<mutex> lock(_mtx);
		cl_int err;
		cl::Buffer outPBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>(range * range * range * sizeof(glm::vec3)),
			pBuffer,
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outPBuffer()");

		cl::Buffer outNBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>(range * range * range * sizeof(float)),
			nBuffer,
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outNBuffer()");

		//Grab perm table for current seed
		//vector<int> seedBuffer = _game.noise().getPerms(seed);

		vector<int>& seedBuffer = _game.noise().getPermBundle();
		int numPerms = 1000 * 512;

		cl::Buffer outSeedBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>(numPerms * sizeof(int)),
			&seedBuffer[0],
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outSeedBuffer()");

		cl::Kernel kernel(_program, "produceChunk3DSimpleSeeded", &err);
		uint32_t ii = 0;
		err |= kernel.setArg(ii++, outPBuffer);
		err |= kernel.setArg(ii++, outNBuffer);
		err |= kernel.setArg(ii++, outSeedBuffer);
		err |= kernel.setArg(ii++, pos);
		err |= kernel.setArg(ii++, resolution);
		err |= kernel.setArg(ii++, range);
		checkErr(err, "Kernel::setArg()");



		cl::CommandQueue queue(_context, _devices[0], 0, &err);
		checkErr(err, "produceChunk3DSimple::CommandQueue::CommandQueue()");
		cl::Event event;
		err = queue.enqueueNDRangeKernel(
			kernel,
			cl::NullRange,
			cl::NDRange((size_t)range, (size_t)range, (size_t)range),
			cl::NullRange,
			NULL,
			&event);
		checkErr(err, "produceChunk3DSimple::ComamndQueue::enqueueNDRangeKernel()");


		event.wait();

		err = queue.enqueueReadBuffer(
			outPBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * range * sizeof(glm::vec3)),
			pBuffer);
		checkErr(err, "ComamndQueue::enqueueReadBuffer::outPBuffer()");

		err = queue.enqueueReadBuffer(
			outNBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * range * sizeof(float)),
			nBuffer);
		checkErr(err, "produceChunk3DSimple::ComamndQueue::enqueueReadBuffer::outNBuffer()");
	}

	void OpenCLManager::heightMapSimpleSeeded(
		glm::dvec3* pBuffer, glm::dvec3 pos, double resolution, double range )
	{
		lock_guard<mutex> lock(_mtx);
		cl_int err;
		cl::Buffer outPBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>(range * range * sizeof(glm::dvec3)),
			pBuffer,
			&err);
		checkErr(err, "heightMapSimple::Buffer::outPBuffer()");

		//PermBundle
		vector<int>& seedBuffer = _game.noise().getPermBundle();
		int numPerms = 1000 * 512;

		cl::Buffer outSeedBuffer(
			_context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			static_cast<size_t>(numPerms * sizeof(int)),
			&seedBuffer[0],
			&err);
		checkErr(err, "produceChunk3DSimple::Buffer::outSeedBuffer()");

		cl::Kernel kernel(_program, "heightMapSimpleSeeded", &err);
		uint32_t ii = 0;
		err |= kernel.setArg(ii++, outPBuffer);
		err |= kernel.setArg(ii++, outSeedBuffer);
		err |= kernel.setArg(ii++, pos);
		err |= kernel.setArg(ii++, resolution);
		err |= kernel.setArg(ii++, range);
		checkErr(err, "heightMapSimple::Kernel::setArg()");


		cl::CommandQueue queue(_context, _devices[0], 0, &err);
		checkErr(err, "heightMapSimple::CommandQueue::CommandQueue()");
		cl::Event event;
		err = queue.enqueueNDRangeKernel(
			kernel,
			cl::NullRange,
			cl::NDRange((size_t)range, (size_t)range),
			cl::NullRange,
			NULL,
			&event);
		checkErr(err, "heightMapSimple::ComamndQueue::enqueueNDRangeKernel()");


		event.wait();

		err = queue.enqueueReadBuffer(
			outPBuffer,
			CL_TRUE,
			0,
			static_cast<size_t>(range * range * sizeof(glm::dvec3)),
			pBuffer);
		checkErr(err, "heightMapSimple::ComamndQueue::enqueueReadBuffer::outPBuffer()");
	}

}
