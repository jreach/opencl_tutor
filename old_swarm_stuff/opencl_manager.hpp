#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <mutex>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <CL/cl.hpp>
#include <glm/glm.hpp>


namespace swarm {
	using namespace std;

	class Game;

	//TODO: Needs Gui integration for full blown hardware inspection and configuration and stuff
	//		A lot of work but not necessary for now
	class OpenCLManager {

	public:
		OpenCLManager(Game& game, int chunkSide);

		void produceChunk3DSimple(
			glm::vec3* pBuffer, float* nBuffer,
			glm::dvec3 pos, double resolution, double range);

		void filterChunk3DSimple(
			glm::vec3* pBuffer, float* nBuffer, char* sBuffer,
			glm::dvec3 pos, double resolution, double range);

		void heightMapSimple( glm::dvec3* pBuffer, glm::dvec3 pos, double resolution, double range);

		void produceChunk3DSimpleSeeded(
			glm::vec3* pBuffer, float* nBuffer,
			glm::dvec3 pos, double resolution, double range);

		void heightMapSimpleSeeded(glm::dvec3* pBuffer, glm::dvec3 pos, double resolution, double range );

	private:
		void checkErr(cl_int err, const char * name);
		void compileProgram(string name);
		cl::Context* getContext();

		const int _chunkSide;
		const int _chunkSize;

		cl_int _err;
		vector< cl::Platform > _platformList;
		cl::Context _context;
		vector<cl::Device> _devices;
		cl::Program _program;

		//cl::Kernel _kernel;
		//unique_ptr<float> _noise_buffer;
		//cl::Buffer _outCL;

		ofstream _log;

		Game& _game;
		mutex _mtx;

	};
}
